function string2(address) {
  address_array = address.split(".");
  array_numbers = address_array.map((each) => Number(each));

  for (let eachValue of array_numbers) {
    if (isNaN(eachValue)) {
      return [];
    }
  }
  return array_numbers;
}

module.exports = string2;
