function string4(title_case_object) {
  array_values = Object.values(title_case_object);
  lower_case = array_values.map((each) => each.toLowerCase());
  title_case = lower_case.map((each) => each[0].toUpperCase() + each.slice(1));
  return title_case;
}
module.exports = string4;
