function string5(array) {
  if (array.length !== 0) {
    return array.join(" ");
  }
  return [];
}
module.exports = string5;
