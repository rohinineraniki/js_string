function string3(date) {
  date_array = date.split("/");
  if (date_array.length === 3 && isNaN(Number(date_array[0])) === false) {
    return date_array[1];
  }
}

module.exports = string3;
